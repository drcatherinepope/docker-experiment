# Docker Experiment

This pipeline builds a very basic Docker image and pushes it to Docker Hub.

The commented-out parts of the pipeline push the image to the GitLab container registry, using predefined variables.